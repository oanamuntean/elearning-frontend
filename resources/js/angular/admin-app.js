var adminApp = angular.module("adminApp", ['ngRoute', 'ngStorage']);

adminApp.config(function ($routeProvider) {
    $routeProvider
        .when('/users', {
            title: 'Management useri',
            templateUrl: 'templates/users.html',
            controller: 'UsersController'
        })
        .when('/add-user', {
            title: 'Adauga user',
            templateUrl: 'templates/add-user.html',
            controller: 'UsersController'
        })
        .when('/permissions', {
            title: 'Permisiuni',
            templateUrl: 'templates/permissions.html',
            controller: 'PermissionsController'
        })
        .when('/logs', {
            title: 'Log-uri',
            templateUrl: 'templates/logs.html',
            controller: 'LogsController'
        })
        .otherwise({
            redirectTo: '/users'
        });
});

adminApp.run(['$rootScope', '$route', function ($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function () {
        document.title = $route.current.title;
    });
}]);


adminApp.factory('UserService', function ($localStorage) {
    return {
        getUser: function () {
            return $localStorage.user;
        },
        getToken: function () {
            return $localStorage.token;
        }
    }
});

adminApp.controller('UsersController', function ($scope, $http, UserService) {
    $scope.users = [];
    $scope.user={};
    $scope.sortOrder=1;
    $scope.selectOption = '';
    $scope.inputText = '';
    $scope.originalUsers = {};

    $http.get("http://localhost:60163/users/details", {
        headers: {
            "Token": UserService.getToken()
        }
    }).success(function (data) {
        for(var i=0;i<data.length;i++){
            if(data[i].Username=="admin"){
                data.splice(i, 1);
                break;
            }
        }
        $scope.users = data;
        $scope.originalUsers=data;
    });

    $scope.addUser = function () {
        $scope.user.Suspended="false";
        $scope.user.Blocked="false";

        if($scope.user.Gender=="F"){
            $scope.user.Gender=true;
        }else{
            $scope.user.Gender=false;
        }

        $http.post("http://localhost:60163/users", $scope.user, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.users.push(data);
                alert("Utilizatorul a fost adaugat!")
            })
            .error(function () {
                alert("Eroare add user!")
            });
    };

    $scope.changeSuspend = function(index){
        var userId = $scope.users[index].Id;
        $http.get("http://localhost:60163/users/"+userId+"/suspend", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                if($scope.users[index].Suspended==true){
                    alert("Utilizatorul a fost suspendat pentru o luna!")
                }else{
                    alert("Utilizatorul nu mai este suspendat!")
                }
            });
    };

    $scope.changeBlock = function(index){
        var userId = $scope.users[index].Id;
        $http.get("http://localhost:60163/users/"+userId+"/block", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                if($scope.users[index].Blocked==true){
                    alert("Utilizatorul a fost blocat!")
                }else{
                    alert("Utilizatorul a fost deblocat!")
                }
            });
    };

    $scope.resetPassword = function(index){
        var userId = $scope.users[index].Id;
    };

    $scope.sortColumn = function (columnName) {
        var order = $scope.sortOrder;
        var users = $scope.users.sort(function (item1, item2) {
            var a = item1[columnName] || '';
            var b = item2[columnName] || '';
            return (a > b ? 1 : a == b ? 0 : -1) * order;
        });
        this.sortOrder = order * -1;
    };

    $scope.search = function(){
        var inputText = $scope.inputText;
        var new_users = $scope.originalUsers.reduce(function(acc, user){
            const u = JSON.stringify(user);
            if (u.indexOf(inputText) >= 0) {
                acc.push(user);
            }
            return acc;
        }, [])
        $scope.users = new_users;
    };

    $scope.restore = function(){
        $scope.users = $scope.originalUsers;
    };

    $scope.setProperty = function(propName, event){
        var value = event.target.value;
        $scope.inputText = value;

    }

});


adminApp.controller('PermissionsController', function ($scope, $http, UserService, $location) {
    $scope.courses = [];
    $scope.permission = {};
    $scope.users=[];
    $scope.permissions=[];
    $scope.roles =['Profesor','Asistent','Student'];

    var getPermissions=function(){
        $http.get("http://localhost:60163/courses/permissions", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.permissions = data;
            })
            .error(function () {
                console.log("Eroare get courses!")
            });
    };
    getPermissions();

    $http.get("http://localhost:60163/courses", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });

    $http.get("http://localhost:60163/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    $scope.addPermission = function () {
        $http.post("http://localhost:60163/api/permissions", $scope.permission, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                alert("Permisiunea a fost adaugata!");
                getPermissions();
            })
            .error(function () {
                console.log("Eroare add permission!")
            });
    }
});

adminApp.controller('LogsController', function ($scope, $http, UserService) {
    $scope.logs = [];
    $scope.log = {};

    $http.get("http://localhost:60163/api/logs", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.logs = data;
        });
});

adminApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (e) {
            if (e.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
});