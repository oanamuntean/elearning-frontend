var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csslint = require('gulp-csslint');
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify');
var imagemin = require("gulp-imagemin");
var concat = require('gulp-concat');
var order = require("gulp-order");

// Configure the proxy server for livereload
var proxyServer = "http://localhost:63342/elearning-frontend",
    port = 3000;

// Array of files to watch
var arrAddFiles = [
    //build_path+'*'
];

// browser-sync task for starting the server.
gulp.task('browser-sync', function () {
    browserSync({
        proxy: proxyServer,
        port: port,
        files: arrAddFiles,
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        },
        notify: false,
        open: false
    });
});

gulp.task('sass', function () {
    return gulp
        .src('resources/scss/style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('build'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('js-angular', function () {
    return gulp
        .src(['resources/js/angular/*.js','resources/js/angular/controllers/*.js'])
        .pipe(concat('angular-app.js'))
        .pipe(gulp.dest('build'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('js-plugins', function () {
    return gulp
        .src('resources/js/plugins/*.js')
        .pipe(order([
            'resources/js/plugins/angular.js',
            'resources/js/plugins/angular-route.js',
            'resources/js/plugins/angular-storage.js',
            'resources/js/plugins/angular-sanitize.js',
            'resources/js/plugins/jquery.js',
            'resources/js/plugins/jquery.slimscroll.min.js',
            'resources/js/plugins/fullcalendar.min.js',
            'resources/js/plugins/moment.min.js',
            'resources/js/plugins/ro.js',
            'resources/js/plugins/bootstrap.js',
            'resources/js/plugins/scrollglue.js',
            'resources/js/plugins/trumbowyg.min.js'
        ], { base: './' }))
        .pipe(concat('plugins.js'))
        .pipe(uglify())
        .pipe(gulp.dest('build'))
        .pipe(browserSync.reload({stream: true}));
});


gulp.task('img', function () {
    return gulp.src('resources/img/*')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('build/img'))
        .pipe(browserSync.reload({stream: true}));
});

//Watch task
gulp.task('watch', function () {
    gulp.watch('resources/scss/*.scss', ['sass']);
    gulp.watch('resources/js/angular/*.js', ['js-angular']);
    gulp.watch('resources/js/angular/controllers/*.js', ['js-angular']);
    gulp.watch('resources/js/plugins/*.js', ['js-plugins']);
    gulp.watch('resources/img/**', ['img']);
});

gulp.task('default', ['browser-sync', 'sass', 'js-angular', 'js-plugins', 'img', 'watch']);