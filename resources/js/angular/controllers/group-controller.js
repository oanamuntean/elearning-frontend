userApp.controller('GroupController', function (UserService, $scope, $http, $location) {
    $scope.groupId = UserService.getUser().group_id;
    $scope.group = {};
    $scope.users = [];
    $scope.pages = [];
    $scope.announcements = [];
    $scope.announcement = {};

    $http.get("http://localhost:60163/groups/" + $scope.groupId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.group = data;
        });

    //get users
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/users", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.users = data;
        });

    //get announcements
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/announcements", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.announcements = data;
        });

    //get pages
    $http.get("http://localhost:60163/groups/" + $scope.groupId + "/pages", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.pages = data;
        });


    $scope.addAnnouncement = function () {
        $scope.announcement.group_id = $scope.groupId;

        $http.post("http://localhost:60163/announcements", $scope.announcement, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.announcements.push(data);
                $scope.announcement={};
            });
    };

    $scope.selectedPage = function (index) {
        var pageId = $scope.pages[index].Id;
        $location.path('/page/' + pageId);
    };

    $scope.seeUserProfile = function (index) {
        var id = $scope.users[index].Id;
        $location.path('/profile/' + id);
    };

});