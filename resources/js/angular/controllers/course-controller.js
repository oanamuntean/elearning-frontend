userApp.controller('CoursesController', function ($scope, $http, $location, UserService) {
    $scope.courses = [];
    $scope.course = {};
    $scope.new_courses = [];

    $http.get("http://localhost:60163/courses/mycourses", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });

    $http.get("http://localhost:60163/courses/others", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.new_courses = data;
        })
        .error(function () {
            console.log("Eroare get courses!")
        });


    $scope.register = function (courses) {
        for (var i in courses) {
            if (courses[i].SELECTED == 'Y') {
                console.log(UserService.getUser().username);
                var permission = {Role: "Student", course_id: courses[i].Id, user_id: UserService.getUser().Id};
                $http.post("http://localhost:60163/api/permissions", permission, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function () {
                        $location.path('/courses');
                    })
                    .error(function () {
                        console.log("Eroare add permission!")
                    });
            }
        }
    }

    $scope.addCourse = function () {
        $http.post("http://localhost:60163/courses", $scope.course, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                var permission = {Role: "Profesor", course_id: data.Id, user_id: UserService.getUser().Id};
                $http.post("http://localhost:60163/api/permissions", permission, {
                        headers: {
                            "Token": UserService.getToken()
                        }
                    })
                    .success(function () {
                    })
                    .error(function () {
                        console.log("Eroare add permission!")
                    });
                $scope.courses.push(data);
            })
            .error(function () {
                console.log("Eroare add course!")
            });
        $location.path('/courses');
    };

    $scope.selectedCourse = function (index) {
        var courseId = $scope.courses[index].Id;
        $location.path('/course/' + courseId);
    };

});

//-------------------------------------------------------------------------------------

userApp.controller('CourseController', function ($scope, $http, $location, $routeParams, UserService) {
    $scope.course = {};
    $scope.module = {};
    $scope.announcement = {};
    $scope.modules = [];
    $scope.discussions = [];
    $scope.announcements = [];
    $scope.courseId = $routeParams.course_id;
    $scope.professor = false;
    $scope.students = [];

    $http.get("http://localhost:60163/courses/" + $scope.courseId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.course = data;
        })
        .error(function () {
            console.log("Eroare get course modules!")
        });

    if (!UserService.getUser().Student) {
        $http.get("http://localhost:60163/courses/" + $scope.courseId + "/users", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.students = data;
            });
    }

    $http.get("http://localhost:60163/courses/mycourses/" + $scope.courseId + "/permission", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            if (data.Role === "Profesor") {
                $scope.professor = true;
            }

        })
        .error(function () {
            console.log("Eroare get permissions!")
        });

    var getModules = function () {
        $http.get("http://localhost:60163/courses/" + $scope.courseId + "/modules", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.modules = data;
            })
            .error(function () {
                console.log("Eroare get course modules!")
            });
    }
    getModules();

    $http.get("http://localhost:60163/courses/" + $scope.courseId + "/discussions", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.discussions = data;
        })
        .error(function () {
            console.log("Eroare get course discussions!")
        });

    $http.get("http://localhost:60163/courses/" + $scope.courseId + "/announcements", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.announcements = data;
        })
        .error(function () {
            console.log("Eroare get course announcements!")
        });

    $scope.addModule = function () {
        $scope.module.course_id = $scope.courseId;
        $scope.module.Content = $('.text-editor').trumbowyg('html');
        console.log($scope.module.content);
        $http.post("http://localhost:60163/modules", $scope.module, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                getModules();
                $location.path('/course/' + $scope.courseId);
            })
            .error(function () {
                console.log("Eroare add module!")
            });

        $location.path('/course/' + $scope.courseId);
    };

    $scope.addDiscussion = function () {
        $scope.discussion.course_id = $scope.courseId;

        $http.post("http://localhost:60163/forum", $scope.discussion, {
            headers: {
                "Token": UserService.getToken()
            }
        }).success(function (data) {
                $scope.discussions.push(data);
                $scope.discussion = {};
            })
            .error(function () {
                console.log("Eroare add discussion!")
            });
    };

    $scope.addAnnouncement = function () {
        $scope.announcement.course_id = $scope.courseId;
        $http.post("http://localhost:60163/announcements/" + $scope.courseId + "/announcement", $scope.announcement, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                $scope.announcements.push(data);
                $scope.announcement = {};
            })
            .error(function () {
                console.log("Eroare add annoucement!")
            });
    }

    $scope.addCourseDiscussion = function () {
        $location.path('/add-discussion/' + $scope.courseId);
    };

    $scope.addCourseAnnouncement = function () {
        $location.path('/add-announcement/' + $scope.courseId);
    };

    $scope.addModuleButton = function () {
        $location.path('/add-module/' + $scope.courseId);
    };

    //go to module page
    $scope.selectedModule = function (index) {
        var id = $scope.modules[index].Id;
        $location.path('/module/' + id);
    };

    //go to discussion page
    $scope.selectedDiscussion = function (index) {
        var id = $scope.discussions[index].Id;
        $location.path('/discussion/' + id);
    };

    //go to course modules
    $scope.courseModule = function () {
        $location.path('/course/' + $scope.courseId);
    };

    //go to course discussons
    $scope.courseDiscussions = function () {
        $location.path('/course/' + $scope.courseId + '/discussions');
    };

    //go to course announcements
    $scope.courseAnnouncements = function () {
        $location.path('/course/' + $scope.courseId + '/announcements');
    };

    $scope.courseStudents = function () {
        $location.path('/course/' + $scope.courseId + '/students');
    };

    $scope.seeUserProfile = function (index) {
        var id = $scope.students[index].Id;
        $location.path('/profile/' + id);
    };

});

//-------------------------------------------------------------------------------------

userApp.controller('ModuleController', function ($scope, $http, $location, $routeParams, UserService, $sce) {
    $scope.module = {};
    $scope.tests = [];
    $scope.homeworks = [];
    $scope.moduleId = $routeParams.module_id || "";
    $scope.professor = false;

    $http.get("http://localhost:60163/modules/" + $scope.moduleId, {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.module = data;
            $scope.module.Content = $sce.trustAsHtml(data.Content);
            $('.text-editor').trumbowyg('html', $scope.module.Content);
            $scope.getPermission();
        })
        .error(function () {
            console.log("Eroare get modul!")
        });


    $scope.editModule = function () {
        $scope.module.Content = $('.text-editor').trumbowyg('html');

        $http.put("http://localhost:60163/modules/" + $scope.moduleId, $scope.module, {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function () {
                $location.path('/module/' + $scope.moduleId);
            })
            .error(function () {
                console.log("Eroare put page!")
            });
    }

    $scope.openEdit = function () {
        $location.path('/edit-module/' + $scope.moduleId);
    }


    $scope.moduleContent = function () {
        console.log($scope.module.content);
        return $sce.trustAsHtml($scope.module.content);
    };

    $scope.getPermission = function () {
        $http.get("http://localhost:60163/courses/mycourses/" + $scope.module.course_id + "/permission", {
                headers: {
                    "Token": UserService.getToken()
                }
            })
            .success(function (data) {
                if (data.Role === "Profesor") {
                    $scope.professor = true;
                }

            })
            .error(function () {
                console.log("Eroare get permissions!")
            });
    };

    //get test
    $http.get("http://localhost:60163/modules/" + $scope.moduleId + "/tests", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.tests = data;
        })
        .error(function () {
            console.log("Eroare get test!")
        });

    //get hw
    $http.get("http://localhost:60163/modules/" + $scope.moduleId + "/homeworks", {
            headers: {
                "Token": UserService.getToken()
            }
        })
        .success(function (data) {
            $scope.homeworks = data;
        })
        .error(function () {
            console.log("Eroare get hw!")
        });

    $scope.moduleTests = function () {
        $location.path('/module/' + $scope.moduleId + '/teste');
    };

    $scope.moduleHomework = function () {
        $location.path('/module/' + $scope.moduleId + '/teme');
    };

    $scope.moduleCourse = function () {
        $location.path('/module/' + $scope.moduleId);
    };

    $scope.startTest = function (index) {
        var testId = $scope.tests[index].Id;
        $location.path('/test/' + testId);
    }

    $scope.solveHomework = function (index) {
        var hwId = $scope.homeworks[index].Id;
        $location.path('/homework/' + hwId);
    }


    $scope.addTest = function () {
        $location.path('/add-test/' + $scope.moduleId);
    };

    $scope.addHomework = function () {
        $location.path('/add-homework/' + $scope.moduleId);
    };

});
