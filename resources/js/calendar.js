$(document).ready(function () {

    var string =localStorage.getItem('ngStorage-token');
    var token = string.substring(1, string.length-1);

    $.ajax({
        url: "http://localhost:60163/events/myevents",
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", token);
        },
        success: function (response) {
            getEvents(response);
        }
    });

    function getEvents(data) {
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            var event = {title: data[i].Name, start: data[i].End_date, description: data[i].Description};
            $('#calendar').fullCalendar('renderEvent', event, true);
            eventsArray.push(event);
        }

    };

    var eventsArray = [];


    $('#calendar').fullCalendar({
        theme: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        eventLimit: true,
        events: eventsArray,
        timeFormat: 'H:mm',
        eventRender: function(event, element) {
            element.find('.fc-title').append("<br/>" + event.description);
        }
    });


    $("#add-event-button").click(function () {
        var name = $('#event-name').val();
        var date = $('#event-date').val();
        var description = $('#event-description').val();
        var event = {Name: name , End_date: date, Description: description};
        addEvent(event);
    });

    function addEvent(data) {
        $.ajax({
            url: "http://localhost:60163/events",
            type: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", token);
            },
            success: function (response) {
                var event = {title: response.Name, start: response.End_date, description: response.Description};
                $('#calendar').fullCalendar('renderEvent', event, true);
                eventsArray.push(event);
            }
        });

    };

});
